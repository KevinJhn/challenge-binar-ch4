Guideline OOP Challenge Chapter 4

1. Jelaskan pengertiannya!
2. Jelaskan di bagian mana hal-hal dibawah ini diimplentasikan pada kode anda! (kosongkan jika tidak diimplementasikan)

class: templet untuk membuat objek(blueprint), contoh: line 11 - 58
object: implementasi dari class contoh: line 63(let game = new Game(val)) 
method: suatu fungsi atau aksi dari suatu object, contoh: line 59

constructor: sebuah fungsi dalam sebuah class, contoh: line 12
properties: nilai dr object di dalam JavaScript yang dapat dimodifikasi, contoh: line 16


encapsulation: proses menyembunyikan data(privete) agak tidak dpt di akses sembarang orang, contoh: line 13(this.computer dapat di panggil dari class Game)

abstraction: class yang tidak bisa langsung dibuat objectnya, tidak digunakan

inheritance: proses menurunkan member dari suatu class untuk sub-class, tidak digunakan

polymorphism: memungkinkan class untuk memiliki bentuk method dengan nama yang sama dengan parameter yang berbeda, tidak digunakan

visibility modifier: tidak digunakan