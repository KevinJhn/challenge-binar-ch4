const [
    hasil,
    refresh,
] = [
    document.getElementById("hasil"),
    document.getElementById("refresh"),
]

let isReady = true

class Game {
    constructor(player1) {
        this.player1 = player1
        let pool = ["gunting", "batu", "kertas"];
        let rand = Math.floor(Math.random() * pool.length);
        this.computer = pool[rand];
    }
    suit() {
        document.getElementById(this.computer + "2").classList.add("active")

        if (this.player1 == this.computer) {
            return this.draw()
        } else if (this.player1 == "gunting") {
            if (this.computer == "kertas") {
                return this.win()
            } else if (this.computer == "batu") {
                return this.lose()
            }
        } else if (this.player1 == "batu") {
            if (this.computer == "gunting") {
                return this.win()
            } else if (this.computer == "kertas") {
                return this.lose()
            }
        } else if (this.player1 == "kertas") {
            if (this.computer == "batu") {
                return this.win()
            } else if (this.computer == "gunting") {
                return this.lose()
            }
        }
    }
    win() {
        hasil.innerHTML = "Player Win"
        hasil.classList.add("bg-win")
        return "Player Win"
    }
    lose() {
        hasil.innerHTML = "Comp win"
        hasil.classList.add("bg-win")
        return "Computer win"
    }
    draw() {
        hasil.innerHTML = "Draw"
        hasil.classList.add("bg-draw")
        return "Draw"
    }
}
const runGame = function (element) {
    if (isReady) {
        element.classList.add("active")
        let val = element.getAttribute("value")
        let game = new Game(val)
        let result = game.suit()
        console.log("Player:", val, "Comp:", game.computer, "Result:", result)
        isReady = false
    } else {
        alert("Reload please")
    }
}